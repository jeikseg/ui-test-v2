import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';

import { HomeComponent } from './HomeComponents/home/home.component';
import { AppInfo } from './Models/AppInfo.model';
import { MainRullingComponent } from './HomeComponents/main-rulling/main-rulling.component';
import { DataService } from './Services/data.service';
import { HttpClientModule } from '@angular/common/http';
import { NavBarComponent } from './GlobalComponents/nav-bar/nav-bar.component';
import { FooterComponent } from './GlobalComponents/footer/footer.component';
import { CounterComponent } from './GlobalComponents/counter/counter.component';
import { MessageComponent } from './HomeComponents/message/message.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { PreviousRulingsComponent } from './HomeComponents/previous-rulings/previous-rulings.component';
import { RulingCardComponent } from './HomeComponents/previous-rulings/ruling-card/ruling-card.component';
import { AnimatedBarComponent } from './GlobalComponents/animated-bar/animated-bar.component';
import { PastTrialsComponent } from './PastTrialsComponets/past-trials/past-trials.component';
import { MainRulingCommentsComponent } from './HomeComponents/main-ruling-comments/main-ruling-comments.component';
import { CommentCardComponent } from './HomeComponents/main-ruling-comments/comment-card/comment-card.component';

export const appInfo: AppInfo = {
  title: 'Rule of Thumb.'
};

@NgModule({
  declarations: [
    AppComponent,
    NavBarComponent,
    FooterComponent,
    HomeComponent,
    MainRullingComponent,
    CounterComponent,
    MessageComponent,
    PreviousRulingsComponent,
    RulingCardComponent,
    AnimatedBarComponent,
    PastTrialsComponent,
    MainRulingCommentsComponent,
    CommentCardComponent
  ],
  imports: [
    BrowserModule,
    BrowserAnimationsModule,
    AppRoutingModule,
    HttpClientModule
  ],
  providers: [
    DataService,
    { provide: 'AppInfo',    useValue: appInfo }
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
