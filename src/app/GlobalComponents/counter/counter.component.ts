import {
  Component, ElementRef, Input, AfterViewInit, ViewChild, OnChanges, SimpleChanges
} from '@angular/core';

@Component({
  selector: 'app-counter',
  templateUrl: './counter.component.html',
  styleUrls: ['./counter.component.css']
})
export class CounterComponent implements AfterViewInit, OnChanges {
  @Input() digitStyle: any;

  @Input() duration: number = 1000;
  @Input() digit: number;
  @Input() steps: number = 12;
  @ViewChild( 'spanContainer') spanElement: ElementRef;

  animCounter() {
    if (typeof this.digit === 'number') {
      this.counterFunc(this.digit, this.duration, this.spanElement);
    }
  }
  counterFunc(endValue, durationMs, element) {
    const stepCount = Math.abs(durationMs / this.steps);
    const valueIncrement = (endValue - 0) / stepCount;
    const sinValueIncrement = Math.PI / stepCount;
    let currentValue = 0;
    let currentSinValue = 0;
    function step() {
      currentSinValue += sinValueIncrement;
      currentValue += valueIncrement * Math.sin(currentSinValue) ** 2 * 2;
      element.nativeElement.textContent = Math.abs(Math.floor(currentValue));
      if (currentSinValue < Math.PI) {
        window.requestAnimationFrame(step);
      }
    }
    step();
  }

  ngAfterViewInit() {
    if (this.digit) {
      this.animCounter();
    }
  }

  ngOnChanges(changes: SimpleChanges) {
    //check if native element is ready
    if (changes["digit"] && !!this.spanElement) {
      //init animation
      this.animCounter();
    }
  }
}
