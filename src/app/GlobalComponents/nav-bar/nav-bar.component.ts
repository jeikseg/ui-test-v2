import { Component, OnInit, Inject } from '@angular/core';
import { AppInfo } from 'src/app/Models/AppInfo.model';


@Component({
  selector: 'app-nav-bar',
  templateUrl: './nav-bar.component.html',
  styleUrls: ['./nav-bar.component.css']
})
export class NavBarComponent implements OnInit {

  constructor(
    @Inject('AppInfo') public appInfo: AppInfo
  ) { }

  ngOnInit(): void {
  }

}
