import { Component, OnInit, AfterViewInit, OnChanges, Input, ViewChild, ElementRef, SimpleChanges } from '@angular/core';

@Component({
  selector: 'app-animated-bar',
  templateUrl: './animated-bar.component.html',
  styleUrls: ['./animated-bar.component.css']
})
export class AnimatedBarComponent implements OnInit, OnChanges {
  @Input() digitStyle: any;
  @Input() labelsAlign: boolean = true;
  @Input() duration: number = 1000;
  @Input() digit: number;
  @Input() steps: number = 10;
  @ViewChild( 'spanContainer') spanElement: ElementRef;
  animatedValue: number;
  animCounter() {
    if (typeof this.digit === 'number') {
      this.counterFunc(this.digit, this.duration);
    }
  }
  counterFunc(endValue, durationMs) {
    const stepCount = Math.abs(durationMs / this.steps);
    const valueIncrement = (endValue - 0) / stepCount;
    const sinValueIncrement = Math.PI / stepCount;
    let currentValue = 0;
    let currentSinValue = 0;
    let _this = this;
    function step() {
      currentSinValue += sinValueIncrement;
      currentValue += valueIncrement * Math.sin(currentSinValue) ** 2 * 2;
      // element.nativeElement.textContent = Math.abs(Math.floor(currentValue));
      _this.animatedValue =  Math.abs(Math.floor(currentValue));
      if (currentSinValue < Math.PI) {
        window.requestAnimationFrame(step);
      }
    }
    step();
  }

  ngOnInit(){
    this.animCounter();
  }

  ngOnChanges(changes: SimpleChanges) {
    //check if native element is ready
    if (changes["digit"] && !!this.spanElement) {
      //init animation
      this.animCounter();
    }
  }
  getOpositeValue(){
    return Math.abs(this.animatedValue-100);
  }
}
