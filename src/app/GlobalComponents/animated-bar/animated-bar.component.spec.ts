import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AnimatedBarComponent } from './animated-bar.component';

describe('AnimatedBarComponent', () => {
  let component: AnimatedBarComponent;
  let fixture: ComponentFixture<AnimatedBarComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AnimatedBarComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AnimatedBarComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
