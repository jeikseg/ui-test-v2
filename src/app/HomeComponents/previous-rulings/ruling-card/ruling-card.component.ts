import { Component, OnInit, Input } from '@angular/core';
import { RulingData } from 'src/app/Models/Ruling.model';

@Component({
  selector: 'app-ruling-card',
  templateUrl: './ruling-card.component.html',
  styleUrls: ['./ruling-card.component.css'],
})
export class RulingCardComponent implements OnInit {
@Input() rulingData: RulingData;

  constructor() { }

  ngOnInit(): void {
  }

  getPercentage(){
    return Math.floor( this.rulingData.likesRatio * 100);
  }

}
