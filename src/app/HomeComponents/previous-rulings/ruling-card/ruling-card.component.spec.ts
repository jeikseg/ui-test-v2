import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { RulingCardComponent } from './ruling-card.component';

describe('RulingCardComponent', () => {
  let component: RulingCardComponent;
  let fixture: ComponentFixture<RulingCardComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ RulingCardComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(RulingCardComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
