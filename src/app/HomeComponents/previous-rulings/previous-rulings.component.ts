import { Component, OnInit, Input } from '@angular/core';
import { RulingData } from 'src/app/Models/Ruling.model';

@Component({
  selector: 'app-previous-rulings',
  templateUrl: './previous-rulings.component.html',
  styleUrls: ['./previous-rulings.component.css']
})
export class PreviousRulingsComponent implements OnInit {
  @Input() rulingDataList: Array<RulingData>;
  constructor() { }

  ngOnInit(): void {
  }

}
