import { Component, OnInit } from '@angular/core';

import { MainRulling, Vote } from 'src/app/Models/MainRulling.model';
import { DataService } from 'src/app/Services/data.service';
import { MessageData } from 'src/app/Models/Message.Model';
import { RulingData } from 'src/app/Models/Ruling.model';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css']
})
export class HomeComponent implements OnInit {
  // data
  mainRollingData: MainRulling;
  messageData: MessageData;
  rulingDataList: Array<RulingData>;
  // ui state variables
  showComments: boolean = false;

  constructor(private _dataService: DataService) { }

  ngOnInit(): void {
    // The real implementation should use an observer and the subscribe method.
    this.mainRollingData = this._dataService.getMainRullingData();
    this.messageData = this._dataService.getHomeMessage();
    this.rulingDataList = this._dataService.getPreviousRulings();

  }

  updateShowComments(data: Vote){
    if (!!this.mainRollingData){
      // update state if user already voted
      this.showComments = this.mainRollingData.vote !== Vote.none;
    }
  }

}
