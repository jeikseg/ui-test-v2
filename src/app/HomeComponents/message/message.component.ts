import { Component, OnInit, Input, AfterViewInit, AfterViewChecked, AfterContentInit } from '@angular/core';
import { MessageData } from 'src/app/Models/Message.Model';
import { trigger, state, style, transition, animate } from '@angular/animations';

@Component({
  selector: 'app-message',
  templateUrl: './message.component.html',
  styleUrls: ['./message.component.css'],
  animations: [
    // the fade-in/fade-out animation.
    trigger('FadeAnimation', [
      state('in',  style({opacity: 1, transform: 'translateX(0)', visibility: 'visible'})),
      state('out', style({opacity: 0, transform: 'translateX(100%)', visibility: 'hidden', height: '0', })),
      transition('in <=> out', [
        animate('0.6s ease-in-out')
      ]),
    ])
  ]
})
export class MessageComponent implements OnInit, AfterViewInit {
  @Input() messageData: MessageData;
  animationState: string = 'out';
  constructor() { }

  ngOnInit(): void {

  }

  ngAfterViewInit(){
    // wait before trigger de animation
    setTimeout( () => this.animationState = 'in', 100);

  }

  close(): void {
    this.animationState = 'out';
  }
}
