import { Component, OnInit, Input, Output } from '@angular/core';
import { MainRulling, Vote } from 'src/app/Models/MainRulling.model';
import { DataService } from 'src/app/Services/data.service';
import { EventEmitter } from '@angular/core';



@Component({
  selector: 'app-main-rulling',
  templateUrl: './main-rulling.component.html',
  styleUrls: ['./main-rulling.component.css']
})
export class MainRullingComponent implements OnInit {
  @Input() mainRullingData: MainRulling;
  @Output() votedEvent: EventEmitter<any> = new EventEmitter();

  barLabelNgStyle: any = { 'font-size': '3em', 'line-height': '2em'};

  constructor(private _dataService: DataService) { }

  ngOnInit(): void {
  }

  getDaysLeft(): number{
    const date1 = new Date(Date.now());
    const date2 = new Date(this.mainRullingData.date);
    const millisDiff = date2.getTime() - date1.getTime();
    const days = Math.round( millisDiff / (86400000) );
    return days;
  }

  sendVote(userVote: Vote): void{
    this.mainRullingData.vote = userVote;
    // data service must validate for duplicated votes, agains the session id. This also include the anonimous sessions.
    const resp = this._dataService.updateMainRulling(this.mainRullingData);
    // check response, and execute actions based on the response.
    if (resp){
      // success logic

      this.votedEvent.emit(userVote);
    }
  }

  like(event: Event){
    this.sendVote(Vote.like);
  }

  dislike(event: Event){
    this.sendVote(Vote.dislike);
  }

  getPercentage(): number{
    return  Math.floor(this.mainRullingData.likesRatio * 100);
  }

}
