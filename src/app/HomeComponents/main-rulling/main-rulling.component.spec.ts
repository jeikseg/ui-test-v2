import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { MainRullingComponent } from './main-rulling.component';

describe('MainRullingComponent', () => {
  let component: MainRullingComponent;
  let fixture: ComponentFixture<MainRullingComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ MainRullingComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(MainRullingComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
