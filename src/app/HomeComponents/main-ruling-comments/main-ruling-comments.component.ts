import { Component, OnInit, Input } from '@angular/core';
import { MainRulling, Vote } from 'src/app/Models/MainRulling.model';
import { CommentData } from 'src/app/Models/Comment.model';
import { DataService } from 'src/app/Services/data.service';

@Component({
  selector: 'app-main-ruling-comments',
  templateUrl: './main-ruling-comments.component.html',
  styleUrls: ['./main-ruling-comments.component.css']
})
export class MainRulingCommentsComponent implements OnInit {

  @Input() mainRollingData: MainRulling;
  isLoading: boolean = false;
  commentsDataList: Array<CommentData>;
  rawCommentsDataList: Array<CommentData>;

  constructor(private _dataService: DataService) { }

  filterType: number = 0;

  ngOnInit(): void {
    // get comments from current poll
    this.rawCommentsDataList = this._dataService.getCommentsFromRuling(this.mainRollingData.id);
    this.filter(0);
  }

  filter(type: number){
    const comments = this.rawCommentsDataList;
    this.filterType = type;
    switch (type) {
      case 1:
        this.commentsDataList  = comments.filter(
          comment => comment.vote === Vote.like);
        break;
      case 2:
        this.commentsDataList  = comments.filter(
          comment => comment.vote === Vote.dislike);
        break;
      case 3:
        this.commentsDataList  = comments.filter(
          comment => comment.vote === Vote.karma);
        break;
      default:
        // all
        this.commentsDataList = comments;
        break;
    }

  }

}
