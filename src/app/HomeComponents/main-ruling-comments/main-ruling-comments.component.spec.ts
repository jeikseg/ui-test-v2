import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { MainRulingCommentsComponent } from './main-ruling-comments.component';

describe('MainRulingCommentsComponent', () => {
  let component: MainRulingCommentsComponent;
  let fixture: ComponentFixture<MainRulingCommentsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ MainRulingCommentsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(MainRulingCommentsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
