import { Component, OnInit, Input } from '@angular/core';
import { CommentData } from 'src/app/Models/Comment.model';
import { Vote } from 'src/app/Models/MainRulling.model';

@Component({
  selector: 'app-comment-card',
  templateUrl: './comment-card.component.html',
  styleUrls: ['./comment-card.component.css']
})
export class CommentCardComponent implements OnInit {

  @Input() commentData: CommentData;

  constructor() { }

  ngOnInit(): void {
  }

  getHeaderColor(){
    switch (this.commentData.vote){
      case Vote.karma:
        return  '#ca6fc3' ;
      case Vote.dislike:
        return '#f0b752';
      case Vote.like:
          return '#1cbbb4';
    }
    return 'grey' ;
  }

  getIcoUrl(){
    switch (this.commentData.vote){
      case Vote.karma:
        return 'assets/images/karma.png';
      case Vote.dislike:
        return 'assets/images/dislike.svg';
      case Vote.like:
          return 'assets/images/like.svg';
    }
  }

}
