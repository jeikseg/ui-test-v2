import { Injectable } from '@angular/core';
import { HttpClient} from '@angular/common/http';
import { Router, ActivatedRoute } from '@angular/router';
import { Observable } from 'rxjs';
import { MainRulling, Vote } from '../Models/MainRulling.model';
import { MessageData } from '../Models/Message.Model';
import { RulingData } from '../Models/Ruling.model';
import { JsonPipe } from '@angular/common';
import { CommentData } from '../Models/Comment.model';

const session_key = 'session';

@Injectable({
  providedIn: 'root'
})
export class DataService {

  private mainRulling: MainRulling;
  private messageData: MessageData;

  previousDemoRulingsJson: string =
  `[
    {
       "id":"2",
       "name":"Kanye West",
       "description":"Vestibulum diam ante, porttitor a odio eget, rhoncus neque. Aenean eu velit libero.",
       "reportId":"34",
       "voteResult":1,
       "date":"1 month",
       "likesRatio":0.97,
       "coverImageUrl":"assets/images/kanye_west_cover.png",
       "moreInfoLink":"#",
       "topicDisplayName":"",
       "category":"Entertainment"
    },
    {
       "id":"3",
       "name":"Mark Zuckerberg",
       "description":"Vestibulum diam ante, porttitor a odio eget, rhoncus neque. Aenean eu velit libero.",
       "reportId":"34",
       "voteResult":0,
       "date":"1 month",
       "likesRatio":0.36,
       "coverImageUrl":"assets/images/mark_cover.png",
       "moreInfoLink":"#",
       "topicDisplayName":"",
       "category":"Business"
    },
    {
       "id":"6",
       "name":"Cristina Fernández de Kirchner",
       "description":"Vestibulum diam ante, porttitor a odio eget, rhoncus neque. Aenean eu velit libero.",
       "reportId":"36",
       "voteResult":0,
       "date":"1 month",
       "likesRatio":0.36,
       "coverImageUrl":"assets/images/cristina_cover.png",
       "moreInfoLink":"#",
       "topicDisplayName":"",
       "category":"Politics"
    },
    {
       "id":"10",
       "name":"Malala Yousafzai",
       "description":"Vestibulum diam ante, porttitor a odio eget, rhoncus neque. Aenean eu velit libero.",
       "reportId":"36",
       "voteResult":0,
       "date":"1 month",
       "likesRatio":0.36,
       "coverImageUrl":"assets/images/malala_cover.png",
       "moreInfoLink":"#",
       "topicDisplayName":"",
       "category":"Entertainment"
    }
 ]`;

 demoCommentsemoJson: string =
  `[
    {
       "id":"2",
       "parentId":"1",
       "author":"Jack",
       "dateText":"2 weeks ago",
       "text":"He’s just another liar with power and a giant cult behind him.",
       "vote":0,
       "myRate":0
    },
    {
      "id":"3",
      "parentId":"1",
      "author":"Tweedy",
      "dateText":"3 weeks ago",
      "text":"More lip service from the Vatican (perhaps a bad choice in terms). Francis is still covering for a Bishop in Argentina, doubt he’ll have the balls to take action. Meanwhile, children will continue to be preyed upon by his “holy” people.",
      "vote":1,
      "myRate":0
    },
    {
      "id":"4",
      "parentId":"1",
      "author":"Mary",
      "dateText":"2 weeks ago",
      "text":"In his next life he will be Pope again, to keep changing Church’s image.",
      "vote":2,
      "myRate":0
    },
    {
      "id":"2",
      "parentId":"1",
      "author":"Jack",
      "dateText":"2 weeks ago",
      "text":"He’s just another liar with power and a giant cult behind him.",
      "vote":0,
      "myRate":0
   },
    {
      "id":"4",
      "parentId":"1",
      "author":"Jack",
      "dateText":"2 weeks ago",
      "text":"He’s just another liar with power and a giant cult behind him.",
      "vote":0,
      "myRate":0
    },
    {
      "id":"3",
      "parentId":"1",
      "author":"Tweedy",
      "dateText":"3 weeks ago",
      "text":"More lip service from the Vatican (perhaps a bad choice in terms). Francis is still covering for a Bishop in Argentina, doubt he’ll have the balls to take action. Meanwhile, children will continue to be preyed upon by his “holy” people.",
      "vote":1,
      "myRate":0
    },
    {
      "id":"4",
      "parentId":"1",
      "author":"Lady G.",
      "dateText":"1 month ago",
      "text":"from the Vatican, nice guy",
      "vote":0,
      "myRate":0
    },
    {
      "id":"4",
      "parentId":"1",
      "author":"Mary",
      "dateText":"2 weeks ago",
      "text":"In his next life he will be Pope again, to keep changing Church’s image.",
      "vote":2,
      "myRate":0
    }
 ]`;

  constructor(private httpClient: HttpClient) {
    // setup service
    // httpClient is injected
    this.createDemoContent();
  }

  createDemoContent(): void{
    this.mainRulling = {
      id: '1',
      name: 'Pope Francis',
      description: 'He’s talking tough on clergy sexual abuse, but is he just another papal pervert protector? (thumbs down) or a true pedophile punishing pontiff? (thumbs up) ',
      vote: Vote.none,
      coverImageUrl: 'assets/images/pope_cover.png',
      date: new Date(2020, 7, 29, 0, 0, 0, 0).toDateString(),
      moreInfoLink: 'https://en.wikipedia.org/wiki/Pope',
      topicDisplayName: 'should be Pope Francis’ Karmic Consequence',
      likesRatio: 0.77,
      category: 'Religion',
    };

    this.messageData = {
      id: '8',
      subTitle: 'Speak out. Be heard.',
      title: 'Be counted',
      text: 'Rule of Thumb is a crowd sourced court of public opinion where anyone and everyone can speak out and speak freely. It’s easy: You share your opinion, we analyze and put the data in a public report.'
    };
  }

  getMainRullingData(): MainRulling{
    // data-service will identify the user, even if It's an anonymous session, with cookies or a GUI.
    return this.mainRulling;
  }

  updateMainRulling( data: MainRulling, userId: string = '0'): boolean{
    // id = 0, is an invalid id.
    // check data
    this.mainRulling = data;
    // return result of the operation. The Error-Message could be implemented.
    return true;
  }

  getHomeMessage(): MessageData{
    // data-service will identify the user, even if It's an anonymous session, with cookies or a GUI.
    return this.messageData;
  }

  getPreviousRulings(): Array<RulingData> {
    // Data comes form demo-json, as it was requested on the test.
    const rulings: Array<RulingData> = JSON.parse(this.previousDemoRulingsJson);
    return rulings;
  }

  getCommentsFromRuling(id: string): Array<CommentData> {
    // Data comes form demo-json, as it was requested on the test.
    const rulings: Array<CommentData> = JSON.parse(this.demoCommentsemoJson);
    return rulings;
  }

}
