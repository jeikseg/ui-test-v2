import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { HomeComponent } from './HomeComponents/home/home.component';
import { PastTrialsComponent } from './PastTrialsComponets/past-trials/past-trials.component';

const routes: Routes = [
  { path: '', component: HomeComponent, pathMatch: 'full' },
  { path: 'past-trials', component: PastTrialsComponent, pathMatch: 'full' },
  // { path: 'how-it-works', component: HowItWorksComponent, pathMatch: 'full' },
  {path: '**', redirectTo:''},
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
