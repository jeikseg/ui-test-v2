/*
 * simple model for "message" or "info" on the home page
 */
export class MessageData {
  id: string;
  subTitle: string;
  title: string;
  text: string;
}
