import { Vote } from './MainRulling.model';

/*
 * model for "Comments" of a poll or "rulig"
 * parentId is the ID of the "poll/votation" that this comment belogns to
 */
export class CommentData {
  id: string;
  parentId: string;
  author: string;
  dateText: string;
  vote: Vote;
  text: string;
  myRate: Vote;
}
