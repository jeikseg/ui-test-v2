/*
 * I have no information about the business-layer, so the model names come from the UI (not the ideal names).
 */
export class MainRulling {
  id: string;
  name: string;
  description: string;
  vote: Vote;
  coverImageUrl: string;
  date: string;
  moreInfoLink: string;
  topicDisplayName: string;
  category: string;
  likesRatio: number;
}

export enum Vote {
  none= -1,
  dislike = 0,
  like = 1,
  karma = 2,
}
