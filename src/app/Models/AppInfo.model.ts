/*
 * This interface defines some global attributes on a sigle place
 */
export interface AppInfo {
  title: string;  // the title-string displayed to user
}
