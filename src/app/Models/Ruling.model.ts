import { Vote } from './MainRulling.model';
/*
 * I have no information about the business-layer, so the model names come from the UI.
 */
export class RulingData {
  id: string;
  name: string;
  voteResult: Vote;
  description: string;
  reportId: string;
  coverImageUrl: string;
  date: string;
  moreInfoLink: string;
  topicDisplayName: string;
  category: string;
  likesRatio: number;
}
