# UI-Test-V2


![Preview](https://bitbucket.org/jeikseg/ui-test-v2/raw/28241620d4c37f8494a85faa1e8fce958e74c364/previews/preview_1.png)

Exercise UI-Test-V2 developed on Angular 9 by Jaekov Segovia

This project was generated with [Angular CLI](https://github.com/angular/angular-cli) version 9.1.2.

## Installation

Angular-cli, node and npm are required. Clone the repository, install node modules with ```npm install``` and finally run 

```bash
ng serve -o
```
## Intro
This is an exercise to demonstrate coding and "logic" skills on a regular development. This means the WebApp is not fully implemented/coded. I focused on the Home-screen, main structure and some of the  key models and components. I also created a "dummy" dataService, to replace the actual back-end. I created a json payload, and I user it to feed the webApp. 

If a real backend-service is implemented, It should implement "observables" instead of consuming the data directly from the service.

I also commented my code, so you can understand better the logic implemented.

## Key Points
I created data models to make easy the process of displaying information on each the angular components. I tried to create all the required components, even if some of them are very simple. I used the angular router-outlet to render the "main-content". There are some animations using angular animation capabilities, and also css3 animations. 

I did not use Less, because the app development is small, and angular encapsulation helps a-lot when it comes to style each component. 

Talking about "the responsive", I used semantic-ui, only the minified-version of the styles. It's a lightweight "stylesheet", suitable for small projects. I also include some explicit media-queries. The Phone-version of the top-bar wasn't included on the ref-files. So, I skip the mobile-menu, but I was aware of it. 
